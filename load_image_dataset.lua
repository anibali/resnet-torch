local dl = require('dataload')
local log = require('log')
local pl = require('pl.import_into')()

local function load_image_dataset(opt)
  assert(pl.path.isdir(opt.train_dir), 'Training example directory does not exist')
  assert(pl.path.isdir(opt.val_dir), 'Validation example directory does not exist')

  local n_threads = 2
  -- approximate size to load the images to before cropping.
  local loadsize = loadsize or {3, 256, 256}
  -- consistent size for cropped patches from loaded images.
  local samplesize = samplesize or {3, 225, 225}

  local sortfunc = function(x,y)
    return x < y
  end

  local train, valid

  if not opt.test_only then
    train = dl.ImageClass(opt.train_dir, loadsize, samplesize, 'sampleTrain', sortfunc, opt.verbose)
    train = dl.AsyncIterator(train, n_threads)
  end

  valid = dl.ImageClass(opt.val_dir, loadsize, samplesize, 'sampleTrain', sortfunc, opt.verbose)
  valid = dl.AsyncIterator(valid, n_threads)

  return train, valid
end

return load_image_dataset
