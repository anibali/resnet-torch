--
--  Copyright (c) 2016, Facebook, Inc.
--  All rights reserved.
--
--  This source code is licensed under the BSD-style license found in the
--  LICENSE file in the root directory of this source tree. An additional grant
--  of patent rights can be found in the PATENTS file in the same directory.
--
--  The training loop and learning rate schedule
--

local optim = require('optim')
local log = require('log')

local M = {}
local Trainer = torch.class('resnet.Trainer', M)

function Trainer:__init(model, criterion, opt, optim_state)
  self.model = model
  self.criterion = criterion

  if opt.optimiser == 'sgd' then
    log.info('Using stochastic gradient decent optimiser')
    self.optim_method = optim.sgd
    self.optim_state = optim_state or {
      learningRate = opt.learning_rate,
      learningRateDecay = 0.0,
      momentum = opt.momentum,
      nesterov = true,
      dampening = 0.0,
      weightDecay = opt.weight_decay,
    }
  elseif opt.optimiser == 'adam' then
    log.info('Using ADAM optimiser')
    self.optim_method = optim.adam
    self.optim_state = optim_state or {
      learningRate = opt.learning_rate,
      beta1 = opt.momentum
    }
  elseif opt.optimiser == 'adadelta' then
    log.info('Using ADADELTA optimiser')
    self.optim_method = optim.adadelta
    self.optim_state = optim_state or {}
  else
    assert(false, 'Unrecognised optimiser: ' .. opt.optimiser)
  end

  self.opt = opt
  self.params, self.gradParams = model:getParameters()
end

function Trainer:train(epoch, dataloader)
  dataloader:reset()

  -- Trains the model for a single epoch
  if self.optim_state.learningRate then
    self.optim_state.learningRate = self:learning_rate_for_epoch(epoch)
  end

  local timer = torch.Timer()
  local dataTimer = torch.Timer()

  local function feval()
    return self.criterion.output, self.gradParams
  end

  local trainSize = 65536
  -- local trainSize = dataloader:size()
  local top1Sum, top5Sum, lossSum = 0.0, 0.0, 0.0
  local N = 0

  log.info('Training epoch # ' .. epoch)
  -- set the batch norm to training mode
  self.model:training()
  for n, inputs, targets in dataloader:sampleiter(self.opt.batch_size, trainSize) do
    local dataTime = dataTimer:time().real

    -- Copy input and target to the GPU
    self:copyInputs(inputs, targets)

    local output = self.model:forward(self.input):float()
    local loss = self.criterion:forward(self.model.output, self.target)

    self.model:zeroGradParameters()
    self.criterion:backward(self.model.output, self.target)
    self.model:backward(self.input, self.criterion.gradInput)

    self.optim_method(feval, self.params, self.optim_state)

    local top1, top5 = self:computeScore(output, targets, 1)
    top1Sum = top1Sum + top1
    top5Sum = top5Sum + top5
    lossSum = lossSum + loss
    N = N + 1

    log.info(('Epoch: [%d][%d/%d]   Time %.3f  Data %.3f  Err %1.4f  top1 %7.3f  top5 %7.3f'):format(
      epoch, n, trainSize, timer:time().real, dataTime, loss, top1, top5))

    -- check that the storage didn't get changed do to an unfortunate getParameters call
    assert(self.params:storage() == self.model:parameters()[1]:storage())

    timer:reset()
    dataTimer:reset()
  end

  return top1Sum / N, top5Sum / N, lossSum / N
end

function Trainer:test(epoch, dataloader)
  dataloader:reset()

  -- Computes the top-1 and top-5 err on the validation set

  local timer = torch.Timer()
  local dataTimer = torch.Timer()
  local size = dataloader:size()

  local nCrops = self.opt.tenCrop and 10 or 1
  local top1Sum, top5Sum = 0.0, 0.0
  local N = 0

  self.model:evaluate()
  for n, inputs, targets in dataloader:subiter(self.opt.batch_size, size) do
    local dataTime = dataTimer:time().real

    -- Copy input and target to the GPU
    self:copyInputs(inputs, targets)

    local output = self.model:forward(self.input):float()
    local loss = self.criterion:forward(self.model.output, self.target)

    local top1, top5 = self:computeScore(output, targets, nCrops)
    top1Sum = top1Sum + top1
    top5Sum = top5Sum + top5
    N = N + 1

    print(('Test: [%d][%d/%d]   Time %.3f  Data %.3f  top1 %7.3f (%7.3f)  top5 %7.3f (%7.3f)'):format(
      epoch, n, size, timer:time().real, dataTime, top1, top1Sum / N, top5, top5Sum / N))

    timer:reset()
    dataTimer:reset()
  end
  self.model:training()

  print(('Finished epoch # %d    top1: %7.3f  top5: %7.3f\n'):format(
    epoch, top1Sum / N, top5Sum / N))

  return top1Sum / N, top5Sum / N
end

function Trainer:computeScore(output, target, nCrops)
  if nCrops > 1 then
    -- Sum over crops
    output = output:view(output:size(1) / nCrops, nCrops, output:size(2))
      --:exp()
      :sum(2):squeeze(2)
  end

  -- Coputes the top1 and top5 error rate
  local batchSize = output:size(1)

  local _ignore, predictions = output:float():sort(2, true) -- descending

  -- Find which predictions match the target
  local correct = predictions:eq(
    target:long():view(batchSize, 1):expandAs(output))

  -- Top-1 score
  local top1 = 1.0 - (correct:narrow(2, 1, 1):sum() / batchSize)

  -- Top-5 score, if there are at least 5 classes
  local len = math.min(5, correct:size(2))
  local top5 = 1.0 - (correct:narrow(2, 1, len):sum() / batchSize)

  return top1 * 100, top5 * 100
end

function Trainer:copyInputs(inputs, targets)
  -- Copies the input to a CUDA tensor, if using 1 GPU, or to pinned memory,
  -- if using DataParallelTable. The target is always copied to a CUDA tensor
  if not self.input then
    if self.opt.n_gpus == 1 then
      self.input = torch.CudaTensor()
    else
      self.input = cutorch.createCudaHostTensor()
    end
  end
  self.target = self.target or torch.CudaTensor()

  self.input:resize(inputs:size()):copy(inputs)
  self.target:resize(targets:size()):copy(targets)
end

function Trainer:learning_rate_for_epoch(epoch)
  -- Training schedule
  local decay = math.floor((epoch - 1) / 40)
  return self.opt.learning_rate * math.pow(0.1, decay)
end

return M.Trainer
