local M = {}

M.verbose = false

function M.info(msg)
  if M.verbose then
    print('[INFO] ' .. tostring(msg))
  end
end

return M
