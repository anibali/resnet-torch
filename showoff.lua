local cjson = require('cjson')
local hc = require('httpclient').new()

-- httpclient is a bad citizen, it overwrites _G._
-- This is a monkey-patch to correct that behaviour.
local hc_client_request = hc.client.request
hc.client.request = function(...)
  local old_ = _G._
  local res = hc_client_request(...)
  _G._ = old_
  return res
end

local M = {}

local Client = {}
Client.__index = Client
M.Client = Client

local Notebook = {}
Notebook.__index = Notebook

local Frame = {}
Frame.__index = Frame

function Client.new(host, port)
  local client = setmetatable({}, Client)
  client.host = host or 'localhost'
  client.port = port or 3000
  return client
end

function Client:notebook(id)
  local notebook = setmetatable({}, Notebook)
  notebook.client = self
  notebook.id = id
  notebook.endpoint = string.format('http://%s:%d/api/notebook/%d', self.host, self.port, id)
  return notebook
end

function Client:create_notebook(title)
  title = title or '<untitled_notebook>'
  local notebook_json = {
    notebook = {
      title = title
    }
  }
  local res = hc:post(string.format('http://%s:%d/api/notebook', self.host, self.port),
    cjson.encode(notebook_json),
    {content_type = 'application/json', ['x-no-compression'] = 'true'})

  local notebook_id
  if res.body then
    notebook_id = cjson.decode(res.body).id
  end

  local notebook = self:notebook(notebook_id)
  notebook.title = title

  return notebook
end

function Notebook:clear()
  local res = hc:delete(self.endpoint .. '/frames',
    {content_type = 'application/json'})
end

function Notebook:frame(id)
  local frame = setmetatable({}, Frame)
  if id then
    frame.id = id
    frame.endpoint = string.format('http://%s:%d/api/frame/%d', self.client.host, self.client.port, id)
  end
  return frame
end

function Notebook:create_frame(title)
  title = title or '<untitled_frame>'
  local frame_json = {
    frame = {
      notebookId = self.id,
      title = title,
      content = {}
    }
  }
  local res = hc:post(string.format('http://%s:%d/api/frame', self.client.host, self.client.port),
    cjson.encode(frame_json),
    {content_type = 'application/json', ['x-no-compression'] = 'true'})

  local frame_id
  if res.body then
    frame_id = cjson.decode(res.body).id
  end

  local frame = self:frame(frame_id)
  frame.title = title

  return frame
end

function Frame:set_title(title)
  self.title = title

  local frame = {
    frame = {
      title = self.title
    }
  }

  local res = hc:put(self.endpoint,
    cjson.encode(frame),
    {content_type = 'application/json'})

  return res
end

function Frame:update(frame_type, body)
  if not self.id then return end

  local frame = {
    frame = {
      type = frame_type,
      content = { body = body }
    }
  }

  local res = hc:put(self.endpoint,
    cjson.encode(frame),
    {content_type = 'application/json'})

  return res
end

function Frame:text(message)
  self:update('text', message)
end

function Frame:html(html)
  self:update('html', html)
end

function Frame:progress(current_value, max_value)
  local percentage = 100 * current_value / max_value
  if percentage > 100 then percentage = 100 end
  local html = string.format([[<div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="%0.2f" aria-valuemin="0" aria-valuemax="100" style="width: %0.2f%%; min-width: 40px;">
    %0.2f%%
  </div>
</div>]], percentage, percentage, percentage)

  self:update('html', html)
end

local html_colours = {
  'steelblue',
  'tomato',
  'olive',
  'blueviolet',
  'darkgreen',
  'sienna',
  'slategrey',
  'crimson'
}

function Frame:graph(xss, yss, opts)
  opts = opts or {}

  local min_x = 99999
  local max_x = -99999
  local min_y = 99999
  local max_y = -99999
  local tables = {}
  local marks = {}
  for i=1,#xss do
    table.insert(marks, {
      type = 'line',
      from = {data = 'table' .. i},
      properties = {
        enter = {
          x = {scale = 'x', field = 'x'},
          y = {scale = 'y', field = 'y'},
          stroke = {value = html_colours[(i - 1) % #html_colours + 1]}
        }
      }
    })
    local points = {}
    for j=1,#xss[i] do
      local x = xss[i][j]
      local y = yss[i][j]
      if x < min_x then min_x = x end
      if x > max_x then max_x = x end
      if y < min_y then min_y = y end
      if y > max_y then max_y = y end
      table.insert(points, {x=x, y=y})
    end
    table.insert(tables, points)
  end

  local data = {}
  for i=1,#tables do
    table.insert(data, {
      name = 'table' .. i,
      values = tables[i]
    })
  end

  local spec = {
    width = 370,
    height = 250,
    data = data,
    scales = {
      {
        name = 'x',
        type = 'linear',
        range = 'width',
        domainMin = min_x,
        domainMax = max_x,
        nice = true,
        zero = false
      },
      {
        name = 'y',
        type = 'linear',
        range = 'height',
        domainMin = opts.y_min or min_y,
        domainMax = opts.y_max or max_y,
        nice = true,
        zero = false
      }
    },
    axes = {
      {type = 'x', scale = 'x', title = opts.x_title},
      {type = 'y', scale = 'y', title = opts.y_title, grid = true}
    },
    marks = marks
  }

  self:update('vega', spec)
end

return M
