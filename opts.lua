local pl = require('pl.import_into')()

local opts_str = [[
Train ResNet
  --share-grad-input  (default "false") Share gradInput to reduce memory usage
  --random-seed       (default 1234)    Seed for random number generators
  --train-dir         (string)          Training example directory
  --val-dir           (string)          Validation example directory
  --output-dir        (string)          Directory to put output files in
  --verbose           (default "true")  Print verbose messages
  --depth             (default 18)      Depth of the network (18, 34, 50, 101, 152, 200)
  --n-gpus            (default 1)       Number of GPUs
  --test-only         (default "false") Skip training
  --n-epochs          (default 1000)    Number of training epochs
  --learning-rate     (default 0.1)     Initial learning rate
  --momentum          (default 0.9)
  --weight-decay      (default 1e-4)
  --optimiser         (default "sgd")   Optimisation algorithm to use (sgd, adadelta, adam)
  --batch-size        (default 32)
  --showoff           (default none)
]]

local bool_opts = {
  'share_grad_input', 'verbose', 'test_only'
}

local opts = {}

function opts.parse(raw_args)
  raw_args = raw_args or _G.args
  local args = pl.lapp.process_options_string(opts_str, raw_args)

  for i,bool_opt in ipairs(bool_opts) do
    if args[bool_opt] ~= 'true' and args[bool_opt] ~= 'false' then
      print(args[bool_opt])
      pl.lapp.error('Expected "true" or "false" for option ' .. bool_opt)
    end
    args[bool_opt] = args[bool_opt] ~= 'false'
  end

  return args
end

return opts
