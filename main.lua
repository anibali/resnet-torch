-- Weird bug alert:
-- Any use of os.execute after requiring cutorch will break CUDA integration.
-- The use of system() seems to be what causes the problem, so we will just
-- replace the implementation of os.execute.
os.execute = function(command)
  if not command then return true end
  local file = io.popen(command)
  local command_output = file:read('*all')
  -- print(command_output)
  return file:close()
end

require('torch')
require('optim')
require('cutorch')

local log = require('log')
local load_image_dataset = require('load_image_dataset')
local models = require('models')
local Trainer = require('train')
local opts = require('opts')
local checkpoints = require('checkpoints')
local showoff = require('showoff')

torch.setdefaulttensortype('torch.FloatTensor')

local opt = opts.parse(_G.arg)
print(opt)

log.verbose = opt.verbose

math.randomseed(opt.random_seed)
torch.manualSeed(opt.random_seed)
cutorch.manualSeedAll(opt.random_seed)

local notebook, losses_frame, errors_frame
if opt.showoff ~= 'none' then
  local _ignore, _ignore, host, port = string.find(opt.showoff, '([^:]+):(%d+)')
  local showoff_client = showoff.Client.new(host, tonumber(port))
  notebook = showoff_client:create_notebook('ResNet')
  losses_frame = notebook:create_frame('Losses during training')
  errors_frame = notebook:create_frame('Top 1 errors during training')
end

-- Load previous checkpoint, if it exists
local checkpoint = checkpoints.latest(opt)
local optim_state = checkpoint and torch.load(checkpoint.optim_file) or nil

local train_loader, val_loader = load_image_dataset(opt)

local n_classes = #(train_loader or val_loader).dataset.classes
log.info('Number of classes: ' .. tostring(n_classes))
opt.n_classes = n_classes

-- Create model
local model, criterion = models.setup(opt, checkpoint)

-- The trainer handles the training loop and evaluation on validation set
local trainer = Trainer(model, criterion, opt, optim_state)

if opt.test_only then
  local top1_err, top5_err = trainer:test(0, val_loader)
  print(string.format(' * Results top1: %6.3f  top5: %6.3f', top1_err, top5_err))
  return
end

local start_epoch = checkpoint and checkpoint.epoch + 1 or 1
local best_test_top1 = math.huge
local best_test_top5 = math.huge
local losses = {}
local top1_errors = {}
for epoch = start_epoch, opt.n_epochs do
  -- Train for a single epoch
  local trainTop1, trainTop5, trainLoss = trainer:train(epoch, train_loader)

  table.insert(losses, trainLoss)
  table.insert(top1_errors, trainTop1)

  if losses_frame then
    losses_frame:graph(
      {torch.range(1, #losses):totable()},
      {losses},
      {x_title = 'Epoch', y_title = 'Loss'})
  end

  if errors_frame then
    errors_frame:graph(
      {torch.range(1, #top1_errors):totable()},
      {top1_errors},
      {x_title = 'Epoch', y_title = 'Top 1 error'})
  end

  -- Run model on validation set
  local test_top1, test_top5 = trainer:test(epoch, val_loader)

  local is_best_model = false
  if test_top1 < best_test_top1 then
    is_best_model = true
    best_test_top1 = test_top1
    best_test_top5 = test_top5
    print(' * Best model ', test_top1, test_top5)
  end

  checkpoints.save(opt, epoch, model, trainer.optim_state, is_best_model)
end

print(string.format(' * Finished top1: %6.3f  top5: %6.3f', best_test_top1, best_test_top5))
