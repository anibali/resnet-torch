# resnet-torch

This is a Torch implementation of [deep residual networks](http://arxiv.org/abs/1512.03385) with [preactivations](http://arxiv.org/abs/1603.05027). It is based on the [Facebook implementation of ResNets](https://github.com/facebook/fb.resnet.torch), but has a few key differences:

* Uses the [Element Research data loader](https://github.com/Element-Research/dataload)
* Can select which optimiser to use (SGD with Nesterov momentum, Adadelta, etc)
* Image dimensions have been tweaked slightly so that the numbers work out nicely during convolution