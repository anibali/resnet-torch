local log = require('log')
local pl = require('pl.import_into')()

local checkpoints = {}

function checkpoints.latest(opt)
  local latest_path = pl.path.join(opt.output_dir, 'latest.t7')
  if not pl.path.isfile(latest_path) then
    return nil
  end

  log.info('Loading checkpoint from ' .. latest_path)

  local latest = torch.load(latest_path)
  latest.model_file = pl.path.join(opt.output_dir, latest.model_file)
  latest.optim_file = pl.path.join(opt.output_dir, latest.optim_file)

  return latest
end

function checkpoints.save(opt, epoch, model, optim_state, is_best_model)
  if torch.type(model) == 'nn.DataParallelTable' then
    model = model:get(1)
  end

  pl.dir.makepath(opt.output_dir)

  -- Only keep 5 most recent checkpoints
  if epoch > 5 then
    local model_file_to_delete =
      pl.path.join(opt.output_dir, 'model_' .. (epoch - 5) .. '.t7')
    local optim_file_to_delete =
      pl.path.join(opt.output_dir, 'optim_state_' .. (epoch - 5) .. '.t7')
    if pl.path.isfile(model_file_to_delete) then
      pl.file.delete(model_file_to_delete)
    end
    if pl.path.isfile(optim_file_to_delete) then
      pl.file.delete(optim_file_to_delete)
    end
  end

  local model_file = 'model_' .. epoch .. '.t7'
  local optim_file = 'optim_state_' .. epoch .. '.t7'

  torch.save(pl.path.join(opt.output_dir, model_file), model)
  torch.save(pl.path.join(opt.output_dir, optim_file), optim_state)
  torch.save(pl.path.join(opt.output_dir, 'latest.t7'), {
    epoch = epoch,
    model_file = model_file,
    optim_file = optim_file,
  })

  if is_best_model then
    torch.save(pl.path.join(opt.output_dir, 'model_best.t7'), model)
  end
end

return checkpoints
