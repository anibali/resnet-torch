--
--  Copyright (c) 2016, Facebook, Inc.
--  All rights reserved.
--
--  This source code is licensed under the BSD-style license found in the
--  LICENSE file in the root directory of this source tree. An additional grant
--  of patent rights can be found in the PATENTS file in the same directory.
--
--  Generic model creating code. For the specific ResNet model see
--  models/resnet.lua
--

require('nn')
require('cunn')
require('cudnn')
local log = require('log')
local pl = require('pl.import_into')()

local models = {}

local Convolution = cudnn.SpatialConvolution
local Avg = cudnn.SpatialAveragePooling
local ReLU = cudnn.ReLU
local Max = nn.SpatialMaxPooling
local SBatchNorm = nn.SpatialBatchNormalization

local function create_model(opt, n_classes)
   local depth = opt.depth
   local shortcutType = opt.shortcutType or 'B'
   local iChannels

   local function ShareGradInput(module, key)
      assert(key)
      module.__shareGradInputKey = key
      return module
   end

   -- The shortcut layer is either identity or 1x1 convolution
   local function shortcut(nInputPlane, nOutputPlane, stride)
      local useConv = shortcutType == 'C' or
         (shortcutType == 'B' and nInputPlane ~= nOutputPlane)
      if useConv then
         -- 1x1 convolution
         return nn.Sequential()
            :add(Convolution(nInputPlane, nOutputPlane, 1, 1, stride, stride))
      elseif nInputPlane ~= nOutputPlane then
         -- Strided, zero-padded identity shortcut
         return nn.Sequential()
            :add(nn.SpatialAveragePooling(1, 1, stride, stride))
            :add(nn.Concat(2)
               :add(nn.Identity())
               :add(nn.MulConstant(0)))
      else
         return nn.Identity()
      end
   end

   -- The basic residual layer block for 18 and 34 layer network, and the
   -- CIFAR networks
   local function basicblock(n, stride, type)
      local nInputPlane = iChannels
      iChannels = n

      local block = nn.Sequential()
      local s = nn.Sequential()
      if type == 'both_preact' then
         block:add(ShareGradInput(SBatchNorm(nInputPlane), 'preact'))
         block:add(ReLU(true))
      elseif type ~= 'no_preact' then
         s:add(SBatchNorm(nInputPlane))
         s:add(ReLU(true))
      end
      s:add(Convolution(nInputPlane,n,3,3,stride,stride,1,1))
      s:add(SBatchNorm(n))
      s:add(ReLU(true))
      s:add(Convolution(n,n,3,3,1,1,1,1))

      return block
         :add(nn.ConcatTable()
            :add(s)
            :add(shortcut(nInputPlane, n, stride)))
         :add(nn.CAddTable(true))
   end

   -- The bottleneck residual layer for 50, 101, and 152 layer networks
   local function bottleneck(n, stride, type)
      local nInputPlane = iChannels
      iChannels = n * 4

      local block = nn.Sequential()
      local s = nn.Sequential()
      if type == 'both_preact' then
         block:add(ShareGradInput(SBatchNorm(nInputPlane), 'preact'))
         block:add(ReLU(true))
      elseif type ~= 'no_preact' then
         s:add(SBatchNorm(nInputPlane))
         s:add(ReLU(true))
      end
      s:add(Convolution(nInputPlane,n,1,1,1,1,0,0))
      s:add(SBatchNorm(n))
      s:add(ReLU(true))
      s:add(Convolution(n,n,3,3,stride,stride,1,1))
      s:add(SBatchNorm(n))
      s:add(ReLU(true))
      s:add(Convolution(n,n*4,1,1,1,1,0,0))

      return block
         :add(nn.ConcatTable()
            :add(s)
            :add(shortcut(nInputPlane, n * 4, stride)))
         :add(nn.CAddTable(true))
   end

   -- Creates count residual blocks with specified number of features
   local function layer(block, features, count, stride, type)
      local s = nn.Sequential()
      if count < 1 then
        return s
      end
      s:add(block(features, stride,
                  type == 'first' and 'no_preact' or 'both_preact'))
      for i=2,count do
         s:add(block(features, 1))
      end
      return s
   end

   local model = nn.Sequential()

    -- Configurations for ResNet:
    --  num. residual blocks, num features, residual block function
    local cfg = {
       [18]  = {{2, 2, 2, 2}, 512, basicblock},
       [34]  = {{3, 4, 6, 3}, 512, basicblock},
       [50]  = {{3, 4, 6, 3}, 2048, bottleneck},
       [101] = {{3, 4, 23, 3}, 2048, bottleneck},
       [152] = {{3, 8, 36, 3}, 2048, bottleneck},
       [200] = {{3, 24, 36, 3}, 2048, bottleneck},
    }

    assert(cfg[depth], 'Invalid depth: ' .. tostring(depth))
    local def, nFeatures, block = table.unpack(cfg[depth])
    iChannels = 64
    log.info('Model type: ResNet-' .. depth)

    -- The ResNet ImageNet model
    model:add(Convolution(3,64,7,7,2,2,3,3))
    model:add(SBatchNorm(64))
    model:add(ReLU(true))
    model:add(Max(3,3,2,2,1,1))
    model:add(layer(block, 64,  def[1], 1, 'first'))
    model:add(layer(block, 128, def[2], 2))
    model:add(layer(block, 256, def[3], 2))
    model:add(layer(block, 512, def[4], 2))
    model:add(ShareGradInput(SBatchNorm(iChannels), 'last'))
    model:add(ReLU(true))
    model:add(Avg(8, 8, 1, 1))
    model:add(nn.View(nFeatures):setNumInputDims(3))
    model:add(nn.Linear(nFeatures, n_classes))

   local function ConvInit(name)
      for k,v in pairs(model:findModules(name)) do
         local n = v.kW*v.kH*v.nOutputPlane
         v.weight:normal(0,math.sqrt(2/n))
         if cudnn.version >= 4000 then
            v.bias = nil
            v.gradBias = nil
         else
            v.bias:zero()
         end
      end
   end
   local function BNInit(name)
      for k,v in pairs(model:findModules(name)) do
         v.weight:fill(1)
         v.bias:zero()
      end
   end

   ConvInit('cudnn.SpatialConvolution')
   ConvInit('nn.SpatialConvolution')
   BNInit('fbnn.SpatialBatchNormalization')
   BNInit('cudnn.SpatialBatchNormalization')
   BNInit('nn.SpatialBatchNormalization')
   for k,v in pairs(model:findModules('nn.Linear')) do
      v.bias:zero()
   end
   model:cuda()

   if opt.cudnn == 'deterministic' then
      model:apply(function(m)
         if m.setMode then m:setMode(1,1,1) end
      end)
   end

   model:get(1).gradInput = nil

   return model
end

function models.setup(opt, checkpoint)
  local n_classes = opt.n_classes or 1000
  local model
  if checkpoint then
    local model_path = checkpoint.model_file
    assert(pl.path.isfile(model_path), 'Saved model not found: ' .. model_path)
    log.info('Resuming model from ' .. model_path)
    model = torch.load(model_path)
  else
    log.info('Creating new model')
    model = create_model(opt, n_classes)
  end

  -- First remove any DataParallelTable
  if torch.type(model) == 'nn.DataParallelTable' then
    model = model:get(1)
  end

  -- This is useful for fitting ResNet-50 on 4 GPUs, but requires that all
  -- containers override backwards to call backwards recursively on submodules
  if opt.share_grad_input then
    local function sharingKey(m)
      local key = torch.type(m)
      if m.__shareGradInputKey then
        key = key .. ':' .. m.__shareGradInputKey
      end
      return key
    end

    -- Share gradInput for memory efficient backprop
    local cache = {}
    model:apply(function(m)
      if torch.isTensor(m.gradInput) and torch.type(m) ~= 'nn.ConcatTable' then
        local key = sharingKey(m)
        if cache[key] == nil then
          cache[key] = torch.CudaStorage(1)
        end
        m.gradInput = torch.CudaTensor(cache[key], 1, 0)
      end
    end)
    for i, m in ipairs(model:findModules('nn.ConcatTable')) do
      if cache[i % 2] == nil then
        cache[i % 2] = torch.CudaStorage(1)
      end
      m.gradInput = torch.CudaTensor(cache[i % 2], 1, 0)
    end
  end

  -- For resetting the classifier when fine-tuning on a different Dataset
  if opt.reset_classifier and not checkpoint then
    log.info('Replacing classifier with ' .. n_classes .. '-way classifier')

    local orig = model:get(#model.modules)
    assert(torch.type(orig) == 'nn.Linear',
      'expected last layer to be fully connected')

    -- Create new linear classification module
    local linear = nn.Linear(orig.weight:size(2), n_classes)
    linear.bias:zero()

    -- Replace old linear classification module
    model:remove(#model.modules)
    model:add(linear:cuda())
  end

  -- Set the CUDNN flags
  if opt.cudnn == 'fastest' then
    cudnn.fastest = true
    cudnn.benchmark = true
  elseif opt.cudnn == 'deterministic' then
    -- Use a deterministic convolution implementation
    model:apply(function(m)
      if m.setMode then m:setMode(1, 1, 1) end
    end)
  end

  -- Wrap the model with DataParallelTable, if using more than one GPU
  if opt.n_gpus > 1 then
    local gpus = torch.range(1, opt.n_gpus):totable()
    local fastest, benchmark = cudnn.fastest, cudnn.benchmark

    local dpt = nn.DataParallelTable(1, true, true)
      :add(model, gpus)
      :threads(function()
        local cudnn = require 'cudnn'
        cudnn.fastest, cudnn.benchmark = fastest, benchmark
      end)
    dpt.gradInput = nil

    model = dpt:cuda()
  end

  local criterion = nn.CrossEntropyCriterion():cuda()
  return model, criterion
end

return models
